<?php

if (!defined('IN_IA')) {

	exit('Access Denied');

}



class Index_EweiShopV2Page extends MobilePage

{

public function success()
{
global $_W;
global $_GPC;	
$url=mobileUrl("member", array(  "id"=>$orderid ), true);

include $this->template();
}

		
public function wechat()
{
global $_W;
global $_GPC;


$uniacid = $_W['uniacid'];
$openid = $_W['openid'];
$params=array(
'title'=>$_GPC['ProductName'],
'uniontid'=>$_GPC['OuttradeNo'],
'fee'=>$_GPC['Price'],
);
$attach=$_GPC['attach'];
load()->func("communication");

load()->model('payment');
$setting = uni_setting($_W['uniacid'], array('payment'));

$sec = m('common')->getSec();
				$sec = iunserializer($sec['sec']);
				
				
			$app_wechat=	$sec['app_wechat'];


$wap_name=$app_wechat['merchname'];
$wechat=array(
'appid'=>$app_wechat['appid'],
'mchid'=>$app_wechat['merchid'],
'apikey'=>$app_wechat['apikey'],

'trade_type'=>'MWEB',
);


	$strs = explode(':', $attach);
      $type= intval($strs[1]);
	  $orderid='0';
	 if($type==0)
	 {
		 
		 $ordersn= substr($params["uniontid"] , 0 , 22);
		 
		$order = pdo_fetch('select * from ' . tablename('ewei_shop_order') . ' where ordersn=:ordersn and uniacid=:uniacid  limit 1', array(':ordersn' =>$ordersn, ':uniacid' => $uniacid));	 
      // print_r($order);
	   $orderid=$order['id'];
	   $params['url_detail']=mobileUrl("order/pay/success", array(  "id"=>$orderid ), true);
	   
	   	
	//echo json_encode($params);
//	exit();
	// return $params;
	 }else
	 {
		
		  $params['url_detail']=mobileUrl("member", array(  "id"=>$orderid ), true);
		}
	  $app_yum='qvtao.com';
	  if(is_ios())
	  { $params['url_detail']=$app_yum.'://'; 
		
 
	  }else
	  {
		 $params['url_detail']=$app_yum.'/'.random(8).'/://'; 
		
		 }
	  
		$package = array( );
		$package["appid"] = $wechat["appid"];
		$package["mch_id"] = $wechat["mchid"];
		$package["nonce_str"] = random(8);
		$package["body"] = cutstr($params["title"], 26);
		$package["attach"] = (string) $attach;
		$package["out_trade_no"] = $params["uniontid"];
		$package["total_fee"] = $params["fee"] * 100;
		$package["spbill_create_ip"] = CLIENT_IP;
		$package["time_start"] = date("YmdHis", TIMESTAMP);
		$package["time_expire"] = date("YmdHis", TIMESTAMP + 600);
		$package["notify_url"] = $_W["siteroot"] . "addons/ewei_shopv2/payment/wechat/notify.php";
        //  $package["REDIRECT_URL"] = "https://www.baidu.com/";
          
		    //$package["redirect_url"] = "https://www.baidu.com/";

		$package["trade_type"] = ($wechat["trade_type"] ? $wechat["trade_type"] : "JSAPI");
		$package["openid"] = (empty($wechat["openid"]) ? $_W["fans"]["from_user"] : $wechat["openid"]);
	
	
	   $package["scene_info"] = json_encode(array( "h5_info" => array( "type" => "Wap", "wap_url" => $_W["siteroot"], "wap_name" => $wap_name ) ));
		
		ksort($package, SORT_STRING);
		$string1 = "";
		foreach( $package as $key => $v ) 
		{
			if( empty($v) ) 
			{
				continue;
			}
			$string1 .= (string) $key . "=" . $v . "&";
		}
		$string1 .= "key=" . $wechat["apikey"];
		$package["sign"] = strtoupper(md5($string1));
		$dat = array2xml($package);
		$response = ihttp_request("https://api.mch.weixin.qq.com/pay/unifiedorder", $dat);
		
		
		$response = ihttp_request("https://api.mch.weixin.qq.com/pay/unifiedorder", $dat);
		if( is_error($response) ) 
		{
			return $response;
		}
		$xml = @isimplexml_load_string($response["content"], "SimpleXMLElement", LIBXML_NOCDATA);
		if( strval($xml->return_code) == "FAIL" ) 
		{
			return error(-1, strval($xml->return_msg));
		}
		if( strval($xml->result_code) == "FAIL" ) 
		{
			return error(-1, strval($xml->err_code) . ": " . strval($xml->err_code_des));
		}
		$prepayid = $xml->prepay_id;
		$wOpt["appId"] = $wechat["appid"];
		$wOpt["timeStamp"] = strval(TIMESTAMP);
		$wOpt["nonceStr"] = random(8);
		$wOpt["package"] = "prepay_id=" . $prepayid;
		$wOpt["signType"] = "MD5";
		ksort($wOpt, SORT_STRING);
		foreach( $wOpt as $key => $v ) 
		{
			$string .= (string) $key . "=" . $v . "&";
		}
		$string .= "key=" . $wechat["apikey"];
		$wOpt["paySign"] = strtoupper(md5($string));
         
		 if( $package["trade_type"] == "MWEB" ) 
		{
			$mweb_url = $xml->mweb_url;
			$wOpt["mweb_url"] = (string) $mweb_url;
		}
	//$params['url_detail']='http://wanxiangcheng365.cn/app/index.php?i=1&c=entry&m=ewei_shopv2&do=mobile&r=member';
$wOpt['mweb_url'] = $wOpt['mweb_url'] . '&redirect_url=' . urlencode($params['url_detail']);	
	$row=array('url'=>$wOpt['mweb_url'],'url_detail'=>$params['url_detail'],'id'=>$orderid,'type'=>$type);
	echo json_encode($row);
	//	return $wOpt;


}
		
public function alipay()
{
global $_W;
global $_GPC;


$sec_yuan = m('common')->getSec();
$sec = iunserializer($sec_yuan['sec']);

$config=$sec['app_alipay'];


$row=array(
'ProductName'=>$_GPC['Desicript'],
'Desicript'=>$_GPC['ProductName'] ,
'OuttradeNo'=>$_GPC['OuttradeNo'],
'total_amount'=>$_GPC['Price'],

);

//print_r($config);
$config['single_alipay_sign_type']=1;
$config['single_private_key']=$config['private_key_rsa2'];
$config['app_id']=$config['appid'];
$pid=$this->singleAliPay($row,$config);
$row=array('pid'=>$pid);

echo  json_encode($row);

}


	public function singleAliPay($row, $config, $remark = 'APP订单支付') 
	{
		global $_W;
		
		if (empty($config['app_id'])) 
		{	
			return error('-1', 'APPID 未配置!');
		}

		$alipay_sign_type = 'RSA2';
		$public = array('app_id' => $config['app_id'], 'method' => 'alipay.trade.app.pay', 'charset' => 'utf-8', 'format' => 'JSON', 'sign_type' => $alipay_sign_type, 'timestamp' => date('Y-m-d H:i:s'), 'version' => '1.0');
		
		
		$biz_content = array();
		
		$biz_content['body'] = $row['ProductName'];
		$biz_content['subject'] = $row['Desicript'];
		$biz_content['out_trade_no'] = $row['OuttradeNo'];
		$biz_content['total_amount'] = $row['total_amount'];
		$biz_content['product_code'] ='QUICK_MSECURITY_PAY';
		$biz_content['timeout_express'] ='30';
	    $notify_url = $_W['siteroot'] . 'addons/ewei_shopv2/payment/alipay/appnotify.php';
		$biz_content['notify_url'] =$notify_url;
		
	/*	$biz_content['out_biz_no'] = $out_biz_no;
		$biz_content['payee_type'] = 'ALIPAY_LOGONID';
		$biz_content['payee_account'] = $aliPayAccount['account'];
		$biz_content['amount'] = $aliPayAccount['money'];
		$biz_content['remark'] = $remark;*/
		
	
		$biz_content = array_filter($biz_content);
		 $public['biz_content'] = json_encode($biz_content);
		$config['private_key'] = $config['single_private_key'];
		
		
		
			
	$params=	$public = $this->alipayRsaSign($public, $config);
	//	ksort($public);
		
		
		foreach ($params as &$value) {
			$value = $this->characet($value, $params['charset']);
		}
		
		
		return http_build_query($params);
	//	echo $this->characet($public,'RSA2');
			exit();
		if ($public['errno'] == '-1') 
		{
			return $public;
		}
		load()->func('communication');
		$url = 'https://openapi.alipay.com/gateway.do';
		
		$response = ihttp_post($url, $public);
		
			print_r($response);
		$result = json_decode(iconv('GBK', 'UTF-8//IGNORE', $response['content']), true);
		
		print_r($result);
			print_r($row);
		exit();
		$query_response = $result['alipay_fund_trans_toaccount_transfer_response'];
			
		
		if ($query_response['code'] != '10000') 
		{
			return error(-1, $query_response['code'] . ' | ' . $query_response['sub_msg']);
		}
		return $query_response;
	}
	
	
		function characet($data, $targetCharset) {
		
		if (!empty($data)) {
			$fileType = 'UTF-8';
			if (strcasecmp($fileType, $targetCharset) != 0) {
				
				$data = mb_convert_encoding($data, $targetCharset, $fileType);
				//				$data = iconv($fileType, $targetCharset.'//IGNORE', $data);
			}
		}


		return $data;
	}
	public function alipayRsaSign($public, $config) 
	{
		if (empty($public)) 
		{
			return error(-1, '参数不能为空！');
		}
		if (!(is_array($public))) 
		{
			return error(-1, '参数格式错误！');
		}
		ksort($public);
		$string1 = '';
		foreach ($public as $key => $v ) 
		{
			if (empty($v)) 
			{
				continue;
			}
			$string1 .= $key . '=' . $v . '&';
		}
		$string1 = rtrim($string1, '&');
		$pkeyid = openssl_pkey_get_private(m('common')->chackKey($config['private_key'], false));
		if ($pkeyid === false) 
		{
			return error(-1, '提供的私钥格式不对');
		}
		$signature = '';
		if ($public['sign_type'] == 'RSA') 
		{
			openssl_sign($string1, $signature, $pkeyid, OPENSSL_ALGO_SHA1);
		}
		else if ($public['sign_type'] == 'RSA2') 
		{
			openssl_sign($string1, $signature, $pkeyid, OPENSSL_ALGO_SHA256);
		}
		openssl_free_key($pkeyid);
		$signature = base64_encode($signature);
		if (empty($signature)) 
		{
			return error(-1, '签名不能为空！');
		}
		$public['sign'] = $signature;
		return $public;
	}

public function wxpay()
{
global $_W;
global $_GPC;


$sec_yuan = m('common')->getSec();
$sec = iunserializer($sec_yuan['sec']);


$Price=$_GPC['Price'];
$row=array(
'apiKey'=>$sec['app_wechat']['appid'],
'mchId'=>$sec['app_wechat']['merchid'],
'partnerKey'=>$sec['app_wechat']['apikey'],
'appsecret'=>$sec['app_wechat']['appsecret'],
'Price'=>$Price*100,
);

echo  json_encode($row);

}

	public function login()

	{

		global $_W;

		global $_GPC;

		
	}



	public function register()

	{

		$this->rf(0);

	}



	public function forget()

	{

		$this->rf(1);

	}



	protected function rf($type)

	{

		global $_W;

		global $_GPC;

		if (is_weixin() || !empty($_GPC['__ewei_shopv2_member_session_' . $_W['uniacid']])) {

			header('location: ' . mobileUrl());

		}



		if ($_W['ispost']) {


$a = range(0,9);
for($i=0;$i<8;$i++){
$b[] = array_rand($a);
}
$mobile=join("",$b);

			 $mobile ='188'.$mobile;

			$verifycode = trim($_GPC['verifycode']);

			$pwd = '123456';



			if (empty($mobile)) {

				show_json(0, '请输入正确的手机号');

			}






			if (empty($pwd)) {

				show_json(0, '请输入密码');

			}



			$key = '__ewei_shopv2_member_verifycodesession_' . $_W['uniacid'] . '_' . $mobile;

		



			$member = pdo_fetch('select id,openid,mobile,pwd,salt from ' . tablename('ewei_shop_member') . ' where mobile=:mobile and mobileverify=1 and uniacid=:uniacid limit 1', array(':mobile' => $mobile, ':uniacid' => $_W['uniacid']));



			if (empty($type)) {

				if (!empty($member)) {

					show_json(0, '此手机号已注册, 请直接登录');

				}



				$salt = empty($member) ? '' : $member['salt'];



				if (empty($salt)) {

					$salt = m('account')->getSalt();

				}



				$openid = empty($member) ? '' : $member['openid'];

				$nickname = empty($member) ? '' : $member['nickname'];



				if (empty($openid)) {

					$openid = 'wap_user_' . $_W['uniacid'] . '_' . $mobile;

					$nickname = substr($mobile, 0, 3) . 'xxxx' . substr($mobile, 7, 4);

				}



				$data = array('uniacid' => $_W['uniacid'], 'mobile' => $mobile, 'nickname' => $nickname, 'openid' => $openid, 'pwd' => md5($pwd . $salt), 'salt' => $salt, 'createtime' => time(), 'mobileverify' => 1, 'comefrom' => 'mobile');

			}

			else {

				if (empty($member)) {

					show_json(0, '此手机号未注册');

				}



				$salt = m('account')->getSalt();

				$data = array('salt' => $salt, 'pwd' => md5($pwd . $salt));

			}



			if (empty($member)) {

				pdo_insert('ewei_shop_member', $data);



				if (method_exists(m('member'), 'memberRadisCountDelete')) {

					m('member')->memberRadisCountDelete();

				}

			}

			else {

				pdo_update('ewei_shop_member', $data, array('id' => $member['id']));

			}



			if (p('commission')) {

				p('commission')->checkAgent($openid);

			}
   
   $member = pdo_fetch('select id,openid,mobile,pwd,salt from ' . tablename('ewei_shop_member') . ' where mobile=:mobile and mobileverify=1 and uniacid=:uniacid limit 1', array(':mobile' => $mobile, ':uniacid' => $_W['uniacid']));

			if (empty($member)) {
				show_json(0, '用户不存在');
			}

			

			m('account')->setLogin($member);



			unset($_SESSION[$key]);

			show_json(1, empty($type) ? '注册成功' : '密码重置成功');

		}



		$sendtime = $_SESSION['verifycodesendtime'];

		if (empty($sendtime) || $sendtime + 60 < time()) {

			$endtime = 0;

		}

		else {

			$endtime = 60 - (time() - $sendtime);

		}



		$set = $this->getWapSet();

		include $this->template('rf', NULL, true);

	}



	public function logout()

	{

		global $_W;

		global $_GPC;

		$key = '__ewei_shopv2_member_session_' . $_W['uniacid'];

		isetcookie($key, false, -100);

		header('location: ' . mobileUrl());

		exit();

	}



	public function sns()

	{

		global $_W;

		global $_GPC;

		if (is_weixin() || !empty($_GPC['__ewei_shopv2_member_session_' . $_W['uniacid']])) {

			header('location: ' . mobileUrl());

		}



		$sns = trim($_GPC['sns']);

		if ( !empty($sns) && !empty($_GPC['openid'])) {

			m('member')->checkMemberSNS($sns);

		}



		if ($_GET['openid']) {

			if ($sns == 'qq') {

				$_GET['openid'] = 'sns_qq_' . $_GET['openid'];

			}



			if ($sns == 'wx') {

				$_GET['openid'] = 'sns_wx_' . $_GET['openid'];

			}



			m('account')->setLogin($_GET['openid']);

		}



		$backurl = '';



		if (!empty($_GPC['backurl'])) {

			$backurl = $_W['siteroot'] . 'app/index.php?' . base64_decode(urldecode($_GPC['backurl']));

		}



		$backurl = empty($backurl) ? mobileUrl(NULL, NULL, true) : trim($backurl);

		header('location: ' . $backurl);

	}



	public function verifycode()

	{

		global $_W;

		global $_GPC;

		@session_start();

		$set = $this->getWapSet();

		$mobile = trim($_GPC['mobile']);

		$temp = trim($_GPC['temp']);

		$imgcode = trim($_GPC['imgcode']);



		if (empty($mobile)) {

			show_json(0, '请输入手机号');

		}



		if (empty($temp)) {

			show_json(0, '参数错误');

		}



		if (!empty($_SESSION['verifycodesendtime']) && time() < $_SESSION['verifycodesendtime'] + 60) {

			show_json(0, '请求频繁请稍后重试');

		}



		if (!empty($set['wap']['smsimgcode'])) {

			if (empty($imgcode)) {

				show_json(0, '请输入图形验证码');

			}



			$imgcodehash = md5(strtolower($imgcode) . $_W['config']['setting']['authkey']);



			if ($imgcodehash != trim($_GPC['__code'])) {

				show_json(-1, '请输入正确的图形验证码');

			}

		}



		$member = pdo_fetch('select id,openid,mobile,pwd,salt from ' . tablename('ewei_shop_member') . ' where mobile=:mobile and mobileverify=1 and uniacid=:uniacid limit 1', array(':mobile' => $mobile, ':uniacid' => $_W['uniacid']));

		if ($temp == 'sms_forget' && empty($member)) {

			show_json(0, '此手机号未注册');

		}



		if ($temp == 'sms_reg' && !empty($member)) {

			show_json(0, '此手机号已注册，请直接登录');

		}



		$sms_id = $set['wap'][$temp];



		if (empty($sms_id)) {

			show_json(0, '短信发送失败(NOSMSID)');

		}



		$key = '__ewei_shopv2_member_verifycodesession_' . $_W['uniacid'] . '_' . $mobile;

		@session_start();

		$code = random(5, true);

		$shopname = $_W['shopset']['shop']['name'];

		$ret = array('status' => 0, 'message' => '发送失败');



		if (com('sms')) {

			$ret = com('sms')->send($mobile, $sms_id, array('验证码' => $code, '商城名称' => !empty($shopname) ? $shopname : '商城名称'));

		}



		if ($ret['status']) {

			$_SESSION[$key] = $code;

			$_SESSION['verifycodesendtime'] = time();

			show_json(1, '短信发送成功');

		}



		show_json(0, $ret['message']);

	}



public function  jpush()
	{
	$alert='5451';
	$title='ewrew';
	$extras=array('url'=>'http://dfdf');
   // $audience ='all';
	
	$audience = array(

			'tag' => array('937bb9d75ba71ac8dae387451606be72')

		);
		
		$jpush_andriod = array(

		'platform' => 'android',

		'audience' => $audience,

		'notification' => array(

			'alert' => $alert,

			'android' => array(

				'alert' => $alert,

				'title' => $title,

				//'builder_id' => 1,

			'extras' => $extras

			)

		),

	);
	print_r($audience);
	
			$jpush_andriod_news = array(

		'platform' => 'android',

		'audience' => $audience,

		'message' => array(

			'msg_content' => $alert,
           // 'builder_id' => 1,

			'title' => $title,

			'extras' => $extras

		),

	);
	
	
		$jpush_ios = array(

		'platform' => 'ios',

		'audience' => $audience,

		'notification' => array(

			'alert' => $alert,

			'ios' => array(

				'alert' => $alert,

				

				'badge' => '+1',

				'extras' => $extras

			),

		),

		'options' => array(

			'apns_production' => 1

		),

	);
	
	$config['push_key']='b2c5bd6aec9c483976937505';
	$config['push_secret']='955fb160b994804fa9e962e2';
	
	//echo  "{$config['push_key']}:{$config['push_secret']}";
		load()->func('communication');

	$extra = array(

		'Authorization' => "Basic " . base64_encode("{$config['push_key']}:{$config['push_secret']}")

	);

	$response = ihttp_request('https://api.jpush.cn/v3/push', json_encode($jpush_andriod), $extra);
     
	 
	$response = ihttp_request('https://api.jpush.cn/v3/push', json_encode($jpush_ios), $extra); 
	 
	print_r($response);
	
	}
	
}	
?>