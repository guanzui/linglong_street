<?php

if (!defined('IN_IA')) {
	exit('Access Denied');
}

class Index_EweiShopV2Page extends MobileLoginPage
{
	public function main()
	{
		global $_W;
		global $_GPC;
		include $this->template();
	}
	
	//获得数据
	public function data()
	{
		global $_W;
		global $_GPC;
		$member_info = pdo_fetch("select * from " . tablename("ewei_shop_member") . " where `openid` = :openid and `uniacid` = :uniacid" , array(":uniacid" => $_W['uniacid'] , ':openid' => $_W['openid']));
		$data = array(
			'nickname' => $member_info['nickname'] ,
			'avatar' => $member_info['avatar'] ? ( strpos( $member_info['avatar'] , 'http' ) !== false ? '' : 'http://' . $_SERVER['HTTP_HOST'] . '/attachment/' . $member_info['avatar'] ) : 'http://' . $_SERVER['HTTP_HOST'] . '/addons/ewei_shopv2/static/images/noface.png' , 
			'integral' => $member_info['credit1'], 
			'status' => 1 ,
			'furit_list' => [],
			'num' => 0,
			'surplus_num' => 0
		);
		
		//获得当前使用的肥料数据
		$fertilizer = pdo_fetch("select * from " . tablename("ewei_shop_games_fertilizer") . " where `openid` = :openid and `status` = 1" , array(":openid" => $_W['openid']));
		if( $fertilizer ) {
			$data['status'] = 2;
			$data['num'] = $fertilizer['num']; //果子数量
			$data['surplus_num'] = pdo_fetchcolumn("select count(id) as total from " . tablename("ewei_shop_games_fruit") . " where `batch_code` = :batch_code and `status` = 2 and `openid` = :openid" , array(":openid" => $_W['openid'],':batch_code' => $fertilizer['batch_code'])); //剩余果子数量
			//获得果子列表
			$data['furit_list'] = pdo_fetchall('select id,integral from ' . tablename('ewei_shop_games_fruit') . ' where `batch_code` = :batch_code and `status` = 1' , array(':batch_code' => $fertilizer['batch_code']));
		}
		//获得肥料数量
		$feinum = pdo_fetchcolumn("select count(id) as total from " . tablename("ewei_shop_games_fertilizer") . " where `status` = 0 and `openid` = :openid" , array(":openid" => $_W['openid']));
		$data['fertilizer'] = intval( $feinum ) ? $feinum : 0;
		$this->ajaxreturn(1,'success' , $data);
	}
	
	//果子摘取
	public function extractfurit()
	{
		global $_GPC;
		global $_W;
		$furit_id = intval( $_GPC['id'] ) ? $_GPC['id'] : $this->ajaxreturn( 0 , '数据错误' );
		$data_info = pdo_fetch("select * from " . tablename("ewei_shop_games_fruit") . " where `uniacid` = :uniacid and `openid` = :openid and id = :id and status = 1" , array(":uniacid" => $_W['uniacid'] , ':id' => $furit_id , ':openid' => $_W['openid']));
		if( !$data_info ) {
			$this->ajaxreturn( 0 , '您要摘取的果子数据有误！' );
		}
		$res = pdo_update("ewei_shop_games_fruit" , ['status' => 2] , ['id' => $furit_id]);
		if( $res ) {
			//写入日志
			pdo_insert("ewei_shop_games_fruit_log",[
				'uniacid' => $data_info['uniacid'],
				'openid' => $data_info['openid'],
				'integral' => $data_info['integral'],
				'batch_code' => $data_info['batch_code'],
				'create_time' => time()
			]);
			//写入用户积分
			$member_info = pdo_fetch("select * from " . tablename("ewei_shop_member") . " where `openid` = :openid and `uniacid` = :uniacid" , array(":uniacid" => $_W['uniacid'] , ':openid' => $_W['openid']));
			pdo_update("ewei_shop_member" , [ 'credit1' => $member_info['credit1'] + $data_info['integral'] ] , [ 'id' => $member_info['id'] ]);
			//写入积分日志
			pdo_insert('mc_credits_record' , [
				'uniacid' => $_W['uniacid'],
				'credittype' => 'credit1',
				'num' => $data_info['integral'],
				'operator' => 0,
				'module' => 'ewei_shopv2',
				'createtime' => time(),
				'remark' => '参与游戏玲珑深林摘取果子获得: '.$data_info['integral'].'积分'
			]);
			pdo_insert('ewei_shop_member_credit_record' , [
				'uniacid' => $_W['uniacid'],
				'openid' => $_W['openid'],
				'credittype' => 'credit1',
				'num' => $data_info['integral'],
				'operator' => 0,
				'module' => 'ewei_shopv2',
				'createtime' => time(),
				'remark' => '参与游戏玲珑深林摘取果子获得: '.$data_info['integral'].'积分'
			]);
			$status = 2;
			//判断是否果子已摘完，摘完了就把状态改成已完成
			$fertilizer = pdo_fetch("select * from " . tablename("ewei_shop_games_fertilizer") . " where `batch_code` = :batch_code and `openid` = :openid and status = 1" , array(":batch_code" => $data_info['batch_code'] , ':openid' => $_W['openid']));
			if( $fertilizer ) {
				$furit_num_total = pdo_fetchcolumn("select count(id) as total from " . tablename("ewei_shop_games_fruit") . " where `batch_code` = :batch_code and status in(0,1)" , array(":batch_code" => $data_info['batch_code']));
				if( !$furit_num_total && !$fertilizer['surplus_num'] ) {
					$status = 1;
					pdo_update('ewei_shop_games_fertilizer' , ['status' => 2] , ['id' => $fertilizer['id']]);
				}
			}
			$this->ajaxreturn(1 , '摘取成功' , ['status' => $status]);
		} else {
			$this->ajaxreturn(0 , '摘取失败，请重试');
		}
	}
	
	//批量摘果子
	public function extractfuritall() {
		global $_GPC;
		global $_W;
		//获得当前进行的批次
		$fertilizer = pdo_fetch("select * from " . tablename("ewei_shop_games_fertilizer") . " where `openid` = :openid and status = 1" , array(':openid' => $_W['openid']));
		if( !$fertilizer ) {
			$this->ajaxreturn( 0 , '果树已枯萎，请给果树施肥！' );
		}
		//获得摘取的果子
		$data_info = pdo_fetchall("select * from " . tablename("ewei_shop_games_fruit") . " where `uniacid` = :uniacid and `openid` = :openid and status = 1" , array(":uniacid" => $_W['uniacid'] , ':openid' => $_W['openid']));
		if( !$data_info ) {
			$this->ajaxreturn( 0 , '您没有果子需要摘取' );
		}
		$status = 2;
		foreach( $data_info as $k => $v ) {
			$res = pdo_update("ewei_shop_games_fruit" , ['status' => 2] , ['id' => $v['id']]);
			if( $res ) {
				//写入日志
				pdo_insert("ewei_shop_games_fruit_log",[
					'uniacid' => $v['uniacid'],
					'openid' => $v['openid'],
					'integral' => $v['integral'],
					'batch_code' => $v['batch_code'],
					'create_time' => time()
				]);
				//写入用户积分
				$member_info = pdo_fetch("select * from " . tablename("ewei_shop_member") . " where `openid` = :openid and `uniacid` = :uniacid" , array(":uniacid" => $_W['uniacid'] , ':openid' => $_W['openid']));
				pdo_update("ewei_shop_member" , [ 'credit1' => $member_info['credit1'] + $v['integral'] ] , [ 'id' => $member_info['id'] ]);
				//写入积分日志
				pdo_insert('mc_credits_record' , [
					'uniacid' => $_W['uniacid'],
					'credittype' => 'credit1',
					'num' => $v['integral'],
					'operator' => 0,
					'module' => 'ewei_shopv2',
					'createtime' => time(),
					'remark' => '参与游戏玲珑深林摘取果子获得: '.$v['integral'].'积分'
				]);
				pdo_insert('ewei_shop_member_credit_record' , [
					'uniacid' => $_W['uniacid'],
					'openid' => $_W['openid'],
					'credittype' => 'credit1',
					'num' => $v['integral'],
					'operator' => 0,
					'module' => 'ewei_shopv2',
					'createtime' => time(),
					'remark' => '参与游戏玲珑深林摘取果子获得: '.$v['integral'].'积分'
				]);
				
				//判断是否果子已摘完，摘完了就把状态改成已完成
				$fertilizer = pdo_fetch("select * from " . tablename("ewei_shop_games_fertilizer") . " where `batch_code` = :batch_code and `openid` = :openid and status = 1" , array(":batch_code" => $v['batch_code'] , ':openid' => $_W['openid']));
				if( $fertilizer ) {
					$furit_num_total = pdo_fetchcolumn("select count(id) as total from " . tablename("ewei_shop_games_fruit") . " where `batch_code` = :batch_code and status in(0,1)" , array(":batch_code" => $v['batch_code']));
					if( !$furit_num_total && !$fertilizer['surplus_num'] ) {
						$status = 1;
						pdo_update('ewei_shop_games_fertilizer' , ['status' => 2] , ['id' => $fertilizer['id']]);
					}
				}
			}
		}
		
		$this->ajaxreturn( 1 , '果子摘取成功' , ['status' => $status] );
	}
	
	//施肥
	public function fertilizer()
	{
		global $_GPC;
		global $_W;
		$fertilizer = pdo_fetch("select * from " . tablename("ewei_shop_games_fertilizer") . " where `openid` = :openid and `status` = 0 order by id asc" , array(":openid" => $_W['openid']));
		if( !$fertilizer ) $this->ajaxreturn( 100 , '抱歉，暂时没有可用的肥料。您可去商城购买产品获得肥料！' );
		//判断是否可以施肥
		if( pdo_fetch("select * from " . tablename("ewei_shop_games_fertilizer") . " where `openid` = :openid and `status` = 1" , array(":openid" => $_W['openid'])) ) {
			$this->ajaxreturn( 0 , '抱歉，果树未枯萎暂时不能施肥。' );
		}
		//使用肥料
		$res = pdo_update("ewei_shop_games_fertilizer" , ['status' => 1 , 'use_time' => time()] , ['id' => $fertilizer['id']]);
		if( $res ) {
			$this->ajaxreturn( 1 , '施肥成功' );
		} else {
			$this->ajaxreturn( 0 , '施肥失败，请重试' );
		}
	}
	//获得摘果日志列表
	public function datalist() {
		global $_W;
		//获取正在使用的批次
		$batch_code = '';
		$data = pdo_fetch("select * from " . tablename("ewei_shop_games_fertilizer") . " where `openid` = :openid and `status` = 1" , array(":openid" => $_W['openid']));
		if( $data ) {
			$batch_code = $data['batch_code'];
		} else {
			$data = pdo_fetch("select * from " . tablename("ewei_shop_games_fertilizer") . " where `openid` = :openid and `status` = 2 order by id desc" , array(":openid" => $_W['openid']));
			if( $data ) {
				$batch_code = $data['batch_code'];
			}
		}
		if( !$batch_code ) $this->ajaxreturn( 1 , 'success' , [] );
		$data = pdo_fetchall("select integral,create_time from " . tablename("ewei_shop_games_fruit_log") . " where `batch_code` = :batch_code order by id desc" , array(":batch_code" => $batch_code));
		if( $data ) {
			foreach( $data as $k => $v ) {
				$data[$k]['create_time'] = date("Y-m-d H:i:s" , $v['create_time']);
			}
		}
		$this->ajaxreturn( 1 , 'success' , $data?$data:[] );
	}
	
	private function ajaxreturn($code = 1,$msg = '' , $data = array()) {
		exit(json_encode(array("code" => $code , 'msg' => $msg , 'data' => $data)));
	}

}

?>
