<?php

if (!defined('IN_IA')) {
	exit('Access Denied');
}

class Games_EweiShopV2ComModel extends ComModel
{
	//订单收货生成未使用的肥料
	public function fertilizerBuild( $order_id ) {
		global $_W;
		if( !$order_id ) return false;
		$order_info = pdo_fetch("select * from " . tablename("ewei_shop_order") . " WHERE `uniacid` = :uniacid AND id = :id" , array(":uniacid" => $_W['uniacid'] , ':id' => $order_id));
		if( !$order_info ) return false;
		//if( $order_info['status'] < 3 ) return false;
		
		//订单收货之后 通过固定比例计算积分加入积分池
		$integral_config = pdo_fetch("select integral_pond_order_scale,integral_pond_price from " . tablename("ewei_shop_integral_pond") . " where uniacid = :uniacid" , array(":uniacid" => $_W['uniacid']));
		if( $integral_config && $integral_config['integral_pond_order_scale'] > 0 ) {
			pdo_update("ewei_shop_integral_pond",[
				'integral_pond_price' => $integral_config['integral_pond_price'] + round( $integral_config['integral_pond_order_scale'] * $order_info['price'] / 100 )
			],['uniacid' => $_W['uniacid']]);
		}
		
		//购物送积分 / 获得系统配置
		$trade_config = m('common')->getSysset('trade');
		if( $trade_config['cashback_integral'] > 0 && $trade_config['cashback_integral'] <= 100 ) {
			$integral = round( $order_info['price'] * $trade_config['cashback_integral'] / 100 , 1);
			if( $integral > 0 ) {
				//写入用户积分
				$member_info = pdo_fetch("select * from " . tablename("ewei_shop_member") . " where `openid` = :openid and `uniacid` = :uniacid" , array(":uniacid" => $_W['uniacid'] , ':openid' => $order_info['openid']));
				pdo_update("ewei_shop_member" , [ 'credit1' => $member_info['credit1'] + $integral ] , [ 'id' => $member_info['id'] ]);
				//写入积分日志
				pdo_insert('mc_credits_record' , [
					'uniacid' => $_W['uniacid'],
					'credittype' => 'credit1',
					'num' => $integral,
					'operator' => 0,
					'module' => 'ewei_shopv2',
					'createtime' => time(),
					'remark' => '购买订单【'.$order_info['ordersn'].'】返: '.$integral.'积分'
				]);
				pdo_insert('ewei_shop_member_credit_record' , [
					'uniacid' => $_W['uniacid'],
					'openid' => $order_info['openid'],
					'credittype' => 'credit1',
					'num' => $integral,
					'operator' => 0,
					'module' => 'ewei_shopv2',
					'createtime' => time(),
					'remark' => '购买订单【'.$order_info['ordersn'].'】返: '.$integral.'积分'
				]);
			}
		}
		
		
		//判断树木是否枯萎 如果枯萎则送肥料
		if( !pdo_fetch("select * from " . tablename("ewei_shop_games_fertilizer") . " where status IN(1,0) and `openid` = :openid AND `uniacid` = :uniacid", array(":uniacid" => $_W['uniacid'] , ':openid' => $order_info['openid']))){
			$batch_code = time() . rand(1000,9999);
			//获得系统配置
			$setting = m('common')->getSysset('games');
			//获得用户信息
			$member_info = pdo_fetch("select * from " . tablename("ewei_shop_member") . " WHERE `openid` = :openid AND `uniacid` = :uniacid" , array(":uniacid" => $_W['uniacid'] , ':openid' => $order_info['openid']));
			if( $member_info ) {
				$res = pdo_insert("ewei_shop_games_fertilizer" , [
					'uniacid' => $order_info['uniacid'],
					'openid' => $order_info['openid'],
					'member_id' => $member_info['id'],
					'order_id' => $order_info['id'],
					'ordersn' => $order_info['ordersn'],
					'batch_code' => $batch_code,
					'scale' => $setting['ratio_order'],
					'num' => $setting['order_num'],
					'surplus_num' => $setting['order_num'],
					'create_time' => time()
				]);
				if( $res ){
					//微信推送
					$remark = "您的果树肥料已送达，您可以去给您的果树施肥啦！";
					$msg = array( "first" => array( "value" => "您的费果树肥料已送达", "color" => "#000000" ), "remark" => array( "value" => $remark, "color" => "#000000" ) );
					m("notice")->sendGamesNotice(array( "openid" => $openid, "tag" => "groups_send", "default" => $msg, "datas" => $datas ));
				}
			}	
		}
		
		$this->noticeFuritMature( $order_id , 1 );
	}
	
	//通知果子成熟了
	public function noticeFuritMature( $order_id , $status ) {
		global $_W;
		if( !$order_id ) return false;
		$furit_data = pdo_fetchall("select * from " . tablename("ewei_shop_games_fruit") . " where order_id = :order_id AND `status` = 0",array(":order_id" => $order_id));
		foreach( $furit_data as $k => $v ) {
			pdo_update("ewei_shop_games_fruit" , ['status' => $status] , ['id' => $v['id']]);
		}
	}
	
	//查询正在生长的果树，并且通知果树开花
	public function noticeFuritFlower( $order_id ) {
		global $_W;
		if( !$order_id ) return false;
		$order_info = pdo_fetch("select * from " . tablename("ewei_shop_order") . " WHERE `uniacid` = :uniacid AND ( id = :id OR ordersn = :ordersn )" , array(":uniacid" => $_W['uniacid'] , ':id' => $order_id , ':ordersn' => $order_id));
		if( !$order_info ) return false;
		$data = pdo_fetchall("select * from " . tablename("ewei_shop_games_fertilizer") . " where `uniacid` = :uniacid AND `status` = 1 AND `surplus_num` > 0" , array(":uniacid" => $_W['uniacid']));
		foreach( $data as $k => $v ) {
			//写入果实开花并且扣除剩余果子
			$surplus_num = $v['surplus_num'] - 1;
			$save_data = ['surplus_num' => $surplus_num];
			if( !$surplus_num ) {
				//$save_data['status'] = 2;
			}
			pdo_update("ewei_shop_games_fertilizer" , $save_data , ['id' => $v['id']]);
			
			//获得积分
			$integral = round( $order_info['price'] * $v['scale'] / 100 , 1);
			if( $integral ) {
				pdo_insert("ewei_shop_games_fruit" , [
					'uniacid' => $v['uniacid'],
					'openid' => $v['openid'],
					'member_id' => $v['member_id'],
					'order_id' => $order_info['id'],
					'ordersn' => $order_info['ordersn'],
					'integral' => $integral ? $integral : 0,
					'status' => 0,
					'batch_code' => $v['batch_code'],
					'create_time' => time()
				]);
			}
		}
	}
}

?>